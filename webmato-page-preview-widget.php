<?php

  /*
	Plugin Name: Webmato Page Preview Widget
	Description: Filters: 'webmato/page-preview-widget_before-title-label', 'webmato/page-preview-widget_after-title-label', 'webmato/page-preview-widget_title_container_class' = ['webmato__page_preview_widget-heading'], 'webmato/page-preview-widget_title-container-tag' = [h2]
	Version: 1.0.0
	Author: Tomas Hrda
	Author URI: http://www.webmato.net
	Repository: https://github.com/webmato/Wordpress-plugin-webmato_page_preview_widget
	Text Domain: webmato__page_preview_widget
	License: MIT
	*/

  /*************************************************************************************************
   *                                     CREATE WIDGET
   ************************************************************************************************/
  class webmato__page_preview_widget extends WP_Widget {

    /***********************************************************************************************
     *                                         VARS
     **********************************************************************************************/

    /**
     * @var string $widget_title
     */
    var $widget_title = 'webmato__page_preview_widget';
    /**
     * @var string $supported_post_types_by_default
     */
    var $supported_post_types_by_default = 'page';
    /**
     * @var array $supported_post_types
     */
    var $supported_post_types;

    /**
     * @var string $base_html_class
     */
    var $base_html_class = 'webmato-page-preview-widget';

    /***********************************************************************************************
     *                                     CONSTRUCTOR
     **********************************************************************************************/
    function __construct(){
      parent::__construct(
        'webmato__page_preview_widget',
        __('Page Preview', 'webmato__page_preview_widget'),
        [
          'description' => __('Panel displays preview of page.', 'webmato__page_preview_widget'),
          'classname'   => $this->base_html_class,
        ]
      );
    }


    /***********************************************************************************************
     *                                     Widget Frontend
     **********************************************************************************************/
    /**
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance){
      //______________________________________________________________________________________________
      // Widget filters
      $supported_post_types = $this->getSupportedPostTypes();
      $before_title_label = apply_filters('webmato/page-preview-widget_before-title-label', null);
      $after_title_label = apply_filters('webmato/page-preview-widget_after-title-label', null);
      $title_container_custom_class = apply_filters('webmato/page-preview-widget_title_container_class', null);
      $title_container_tag = apply_filters('webmato/page-preview-widget_title-container-tag', null);
      $title_custom_class = apply_filters('webmato/page-preview-widget_title_custom_class', null);


      //____________________________________________________________________________________________
      // Widget setting

      $custom_css_class = isset($instance['custom_css_class']) ? sanitize_text_field($instance['custom_css_class']) : '';
      $selected_display_view = $instance['selected_page_preview_content_type'];
      $post_id = $instance['selected_page_id'];

      $query = new WP_Query([
        'p'         => $post_id,
        'post_type' => $supported_post_types,
      ]);

      if ($query->have_posts()){
        while ($query->have_posts()){
          $query->the_post();
          $post = get_post();
          $post_title = get_the_title();

          //____________________________________________________________________________________________
          // Post attributes
          // $page = get_post($post_id);
          $page_permalink = get_permalink();
          $page_title = apply_filters('sublanguage_translate_post_field', $post_title, $post, 'post_title');

          if ( !$title_container_custom_class){
            $title_container_custom_class = '';
          }

          if ( !$title_container_tag){
            $title_container_tag = 'h2';
          }

          if ( !$title_custom_class){
            $title_custom_class = '';
          }


          //____________________________________________________________________________________________
          // Render widget
          ?>
          <!-- WEBMATO.NET - PAGE PREVIEW WIDGET -->
          <?php

          echo $args['before_widget'];


          ?>
          <article id="<?= $this->lastElementOfUrl($page_permalink); ?>"
            <?php post_class($this->base_html_class . '__container') ?>>

            <?php
              //______________________________________________________________________________________
              // Render - Widget Header
            ?>

            <!-- WEBMATO.NET - PAGE PREVIEW WIDGET - HEADER -->
            <?php if ($instance['selected_page_preview_show_heading'] === 'on'): ?>
              <header class="<?= $this->base_html_class . "__header"; ?>">
                <?= '<' . $title_container_tag . ' class="' . $this->base_html_class . '__title-container ' . $title_container_custom_class . '" >' ?>
                <?= $before_title_label ?>
                <span
                  class="<?= $title_custom_class . ' ' . $this->base_html_class . '__title'; ?>">
                  <?php the_title() ?>
                </span>
                <?= $after_title_label ?>
                <?= '</' . $title_container_tag . '>' ?>
              </header>
            <?php endif; ?>
            <!-- /WEBMATO.NET - PAGE PREVIEW WIDGET - HEADER -->


            <?php
              //__________________________________________________________________________________
              // Render - Page View
            ?>
            <div class="<?= $this->base_html_class . "__preview-content " . $custom_css_class; ?>">

              <?php if ($selected_display_view === 'excerpt'): ?>

                <?php
                //__________________________________________________________________________________
                // Page View: Excerpt

                // TODO: only if sublangue plugin exists...
                $page_excerpt = apply_filters('sublanguage_translate_post_field', get_the_excerpt(), $post, 'post_excerpt');
                ?>
                <!-- WEBMATO.NET - PAGE PREVIEW WIDGET - PAGE EXCERPT -->
                <div class="<?= $this->base_html_class . '__page-excerpt'; ?>">
                  <?= $page_excerpt ?>
                </div>
                <span class="<?= $this->base_html_class . '__page-excerpt-link-more link-more'; ?>">
	                  <a href="<?= $page_permalink ?>">
	                    <?= __('more', 'webmato__page_preview_widget') ?>
	                  </a>
                  </span>
                <!-- /WEBMATO.NET - PAGE PREVIEW WIDGET - PAGE EXCERPT -->

              <?php elseif ($selected_display_view === 'content') : ?>

                <?php
                //__________________________________________________________________________________
                // Page View: Content

                // TODO: only if sublangue plugin exists...
                $page_content = apply_filters('sublanguage_translate_post_field', get_the_content(), $post, 'post_content');
                ?>
                <!-- WEBMATO.NET - PAGE PREVIEW WIDGET - PAGE CONTENT -->
                <div class="<?= $this->base_html_class . '__page-entry-content'; ?>">
                  <?= wp_make_content_images_responsive(do_shortcode($page_content)); ?>
                </div>
                <!-- /WEBMATO.NET - PAGE PREVIEW WIDGET - PAGE CONTENT -->

              <?php elseif ($selected_display_view === 'full') : ?>

                <?php
                //__________________________________________________________________________________
                // Page View: Full Page
                ?>

                <!-- WEBMATO.NET - PAGE PREVIEW WIDGET - FULL PAGE -->
                <div class="<?= $this->base_html_class . '__page-full-content'; ?>">
                  <?php get_template_part('single', get_post_type()); ?>
                </div>
                <!-- /WEBMATO.NET - PAGE PREVIEW WIDGET - FULL PAGE -->

              <?php endif; ?>

            </div>


            <?php
              //________________________________________________________________________________________
              // Render - Widget Footer
            ?>
            <!-- WEBMATO.NET - PAGE PREVIEW WIDGET - FOOTER -->
            <footer></footer>
            <!-- /WEBMATO.NET - PAGE PREVIEW WIDGET - FOOTER -->


          </article>

          <?php
          echo $args['after_widget'];
          ?>
          <!-- /WEBMATO.NET - PAGE PREVIEW WIDGET -->
          <?php
        }
      }
      wp_reset_postdata();
    }


    /***********************************************************************************************
     *                                     Widget Backend
     **********************************************************************************************/
    /**
     * @param array $instance
     *
     * @return string|void
     */
    public function form($instance){
      $pages_query = new WP_Query([
        'post_type' => $this->getSupportedPostTypes(),
        'order'     => 'DESC',
        'orderby'   => 'name',
      ]);

      $custom_css_class = isset($instance['custom_css_class']) ? sanitize_text_field($instance['custom_css_class']) : '';

      // TODO: Add custom Title input

      ?>
      <p>
        <label for="<?php echo $this->get_field_id('selected_page_id'); ?>">
          <?php _e('Select Page to show:', 'webmato__page_preview_widget'); ?>
        </label>

        <select
          class="widefat"
          id="<?php echo $this->get_field_id('selected_page_id'); ?>"
          name="<?php echo $this->get_field_name('selected_page_id'); ?>"
        >
          <?php while ($pages_query->have_posts()) : $pages_query->the_post(); ?>
            <?php

            $selected = '';
            if (array_key_exists('selected_page_id', $instance)){
              $selected = selected($instance['selected_page_id'], get_the_ID(), false);
            }
            ?>
            <option value="<?= get_the_ID() ?>" <?= $selected ?>>
              <?= get_the_title() ? the_title() : get_the_ID(); ?>
            </option>
          <?php endwhile; ?>
          <?php
            $selected2 = '';
            if (array_key_exists('selected_page_id', $instance)){
              $selected2 = selected($instance['selected_page_id'], '', false);
            }
          ?>
          <option value="" <?= $selected2 ?>></option>
        </select>
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('selected_page_preview_content_type'); ?>">
          <?php _e('Show page content or page excerpt:', 'webmato__page_preview_widget'); ?>
        </label>
        <select
          class="widefat"
          id="<?php echo $this->get_field_id('selected_page_preview_content_type'); ?>"
          name="<?php echo $this->get_field_name('selected_page_preview_content_type'); ?>"
        >
          <option
            value="excerpt"
            <?= $this->checkSelected($instance, 'selected_page_preview_content_type', 'excerpt') ?>
          >
            Page excerpt
          </option>
          <option
            value="content"
            <?= $this->checkSelected($instance, 'selected_page_preview_content_type', 'content') ?>
          >
            Page content
          </option>
          <option
            value="full"
            <?= $this->checkSelected($instance, 'selected_page_preview_content_type', 'full') ?>
          >
            Full Page View
          </option>
        </select>
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('selected_page_preview_show_heading'); ?>">
          <?php _e('Show heading of the page', 'webmato__page_preview_widget'); ?>
        </label>
        <input
          type="checkbox"
          id="<?php echo $this->get_field_id('selected_page_preview_show_heading'); ?>"
          name="<?php echo $this->get_field_name('selected_page_preview_show_heading'); ?>"
          <?= $this->checkChecked($instance, 'selected_page_preview_show_heading', 'on') ?>
        >
      </p>
      <p>
        <label for="<?php echo $this->get_field_id('selected_page_preview_custom_css_class'); ?>">
          <?php _e('Custom CSS Classes', 'webmato__page_preview_widget'); ?>
        </label>
        <input
          type="text"
          id="<?php echo $this->get_field_id('selected_page_preview_custom_css_class'); ?>"
          name="<?php echo $this->get_field_name('selected_page_preview_custom_css_class'); ?>"
          value="<?= $custom_css_class; ?>"
        >
      </p>
      <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance){
      $instance['selected_page_id'] = $new_instance['selected_page_id'];
      $instance['selected_page_preview_content_type'] = $new_instance['selected_page_preview_content_type'];
      $instance['selected_page_preview_show_heading'] = $new_instance['selected_page_preview_show_heading'];
      $instance['custom_css_class'] = sanitize_text_field($new_instance['selected_page_preview_custom_css_class']);

      return $instance;
    }


    /***********************************************************************************************
     *                                       HELPERS
     **********************************************************************************************/
    protected function getSupportedPostTypes(){
      $supported_post_types = apply_filters('webmato/page-preview-widget_supported-post-types', null);

      if ( !$supported_post_types){
        $this->$supported_post_types = $this->supported_post_types_by_default;
      } else {
        $this->supported_post_types = $supported_post_types;
      }

      return $this->supported_post_types;
    }

    /**
     * @param $instance
     * @param $instance_key
     * @param $option_value
     *
     * @return string
     */
    function checkSelected($instance, $instance_key, $option_value){
      if (array_key_exists($instance_key, $instance)){
        return selected($instance[$instance_key], $option_value, false);
      } else {
        return '';
      }
    }

    /**
     * @param $instance
     * @param $instance_key
     * @param $is_checked
     *
     * @return string
     */
    function checkChecked($instance, $instance_key, $is_checked){
      if (array_key_exists($instance_key, $instance)){
        return checked($instance[$instance_key], $is_checked, false);
      } else {
        return '';
      }
    }

    /**
     * @param {String} $url
     *
     * @return string
     */
    function lastElementOfUrl($url){
      if (substr($url, - 1) === '/'){
        $url = rtrim($url, '/');
      }
      $url_array = explode('/', $url);

      return end($url_array);
    }

  }


  /*************************************************************************************************
   *                                     REGISTER WIDGET
   ************************************************************************************************/
  // Register and load the widget
  add_action('widgets_init', function(){
    register_widget('webmato__page_preview_widget');
  });

  // Load localization files
  add_action('plugins_loaded', function(){
    $plugin_dir = plugin_basename(dirname(__FILE__));
    $basename = basename($plugin_dir);
    $lang_dir = $basename . '/lang/';

    load_plugin_textdomain('webmato__page_preview_widget', false, $lang_dir);
  });


